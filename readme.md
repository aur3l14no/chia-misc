## Usage

### Start all jobs

1. copy `settings.example.py` to `settings.py`, and fill in your settings
2. run `main.py`
3. (TODO) press `enter` to see progress

What the script does is

1. check all temp folders, if contains completed files (but not transfered), warn and exit
2. clear old incomplete temp files
3. fire multiple processes to plot
4. (TODO) when press enter, it scans logs to print a summary

### Stop all jobs

`Ctrl-C`

### Check progress

Run `check.py` to see ongoing progress.

## Details

### Naming conventions

- plot filename: plot-k32-2021-04-21-07-56-5c262b6207b60602ef55bb8336c198bc4efd27fb8fc75ac644abaf430e624aba.plot
- log filename: 2021-04-21-07-56-22.log
- err filename: 2021-04-21-07-56-22.err


## Known Bugs

Can't kill child processes correctly when using `cpulimit`

![](https://stackoverflow.com/questions/53727273/cant-kill-cpulimit-via-psutil)
