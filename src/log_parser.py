import re
import os
from dataclasses import dataclass
from typing import Optional


@dataclass
class LogInfo:
    plot_id: str
    # plot_basename: str
    state: int  # 0: doing phase 1, 1: doing phase 2, ..., 4: done
    time_phase_1: Optional[float]
    time_phase_2: Optional[float]
    time_phase_3: Optional[float]
    time_phase_4: Optional[float]
    total_time: Optional[float]

def parse_log(log_path) -> LogInfo:
    '''Parse log and extract info.

    We pay attention to the following lines:

    ID: 31e15884202548c7de473dbc90ca90e71391917b27aa8ff4584c7be423666666
    Time for phase 1 = 9730.166 seconds. CPU (155.120%) Mon Apr 19 19:18:48 2021
    Time for phase 2 = 3565.294 seconds. CPU (81.930%) Mon Apr 19 20:18:13 2021
    Time for phase 3 = 9837.310 seconds. CPU (71.900%) Mon Apr 19 23:02:10 2021
    Time for phase 4 = 813.306 seconds. CPU (60.060%) Mon Apr 19 23:15:44 2021
    Renamed final file from "E:\\plots\\plot-k32-2021-04-19-16-36-31e15884202548c7de473dbc90ca90e71391917b27aa8ff4584c7be4236696666.plot.2.tmp" to "E:\\plots\\plot-k32-2021-04-19-16-36-31e15884202548c7de473dbc90ca90e71391917b27aa8ff4584c7be423666666.plot"
    Total time = 23946.080 seconds. CPU (106.810%) Mon Apr 19 23:15:44 2021

    Returns:
      a LogInfo object
    '''

    def _extract(pattern, target):
        try:
            ret = re.search(pattern, target).group(1)
            if isinstance(ret, float):
                return float(ret)
            return ret
        except:
            return None
    
    def _update(var, value):
        return value if value else var

    plot_id = time_phase_1 = time_phase_2 = time_phase_3 = time_phase_4 = total_time = finished = None
    with open(log_path) as f:
        for l in f:
            plot_id = _update(plot_id, _extract('ID: (\w+)', l))
            time_phase_1 = _update(time_phase_1, _extract('Time for phase 1 = ([\d\.]+) seconds\.', l))
            time_phase_2 = _update(time_phase_2, _extract('Time for phase 2 = ([\d\.]+) seconds\.', l))
            time_phase_3 = _update(time_phase_3, _extract('Time for phase 3 = ([\d\.]+) seconds\.', l))
            time_phase_4 = _update(time_phase_4, _extract('Time for phase 4 = ([\d\.]+) seconds\.', l))
            total_time = _update(total_time, _extract('Total time = ([\d\.]+) seconds\.', l))
            finished = _update(finished, _extract('(Renamed final file from)', l))

        state = [True if x != None else False for x in [time_phase_1, time_phase_2, time_phase_3, time_phase_4, finished]].count(True)
        return LogInfo(plot_id, state, time_phase_1, time_phase_2, time_phase_3, time_phase_4, total_time)
