import os
import psutil
import shutil


def print_file_tree(path, max_depth=2, max_breadth=10):
    def _print_indent(*args, indent=0, **kwargs):
        print(' '*indent, end='')
        print(*args, **kwargs)

    def _print_subtree(path, max_depth=1, max_breadth=10, indent=0, is_full_path=False):
        _print_indent(path if is_full_path else os.path.basename(
            path), indent=indent)
        if max_depth <= 0:
            return
        if os.path.isdir(path):
            for i, fn in enumerate(os.listdir(path)):
                if i >= max_breadth:
                    break
                _print_subtree(os.path.join(path, fn),
                               max_depth=max_depth-1,
                               max_breadth=max_breadth,
                               indent=indent+2)
    _print_subtree(path, max_depth, max_breadth, is_full_path=True)


def prompt_to_delete(fps, ignore_outer_directories=False):
    '''Prompt to delete all files (fp) in fps'''
    def _delete(fp):
        try:
            if os.path.isfile(fp) or os.path.islink(fp):
                os.unlink(fp)
            elif os.path.isdir(fp):
                shutil.rmtree(fp)
        except Exception as e:
            print('Failed to delete %s. Reason: %s' % (fp, e))

    fps = set(fps)
    for fp in fps:
        print_file_tree(fp)
    if input('Delete? [y/N]: ') in ['y', 'Y']:
        for fp in fps:
            if os.path.isdir(fp) and ignore_outer_directories:
                for inner_fn in os.listdir(fp):
                    inner_fp = os.path.join(fp, inner_fn)
                    _delete(inner_fp)
            else:
                _delete(fp)
    print()

def kill_proc_tree(pid, including_parent=True):    
    '''Kills process tree

    Args:
      including_parent: if it kill the parent process
    '''
    proc = psutil.Process(pid)
    procs = proc.children(recursive=True)
    procs.append(proc)
    print(f'Killing: {procs}')

    for proc in procs:
        proc.terminate()

    gone, alive = psutil.wait_procs(procs, timeout=1)
    for p in alive:
        p.kill()

if __name__ == '__main__':
    # print_file_tree('/Users/anonymz')
    prompt_to_delete(['/Users/anonymz/Temp/ca'])
