import os
import time
import subprocess as sp
import datetime as dt
import psutil
from log_parser import parse_log
from typing import Dict
from job import Job, JobStat

from settings import fingerprint, n_threads, mem_size, log_dir, chia_path, delay, jobs, bucket_size
from shell_utils import kill_proc_tree, prompt_to_delete


def gen_command(temp_dir, dest_dir):
    '''Generate command
       Example: plots create -k 32 -u 128 -r 2 -t F:\plot_temp -d Z:\Mini\plots -n 2 -a 11111111
    '''
    return f'{chia_path} plots create -k 32 -u {bucket_size} -b {mem_size} -r {n_threads} -t {temp_dir} -d {dest_dir} -n 1 -a {fingerprint}'


def clear_temp_dirs(temp_dirs):
    '''Clear orphan temp files and logs.

    1. Check if there's any uncopied but finished plot file in temp dirs.
    2. Clear temp dirs as they should be empty when you start plotting.
    3. Clear orphan logs from log dir as well.

    Returns:
      0: if everything is good.
      1: if there are uncopied but finished plot files.
    '''

    log_fps = [os.path.join(log_dir, fn)
               for fn in os.listdir(log_dir) if fn.endswith('.log')]
    temp_fps = {k: [os.path.join(k, fn) for fn in os.listdir(k)]
                for k in temp_dirs}

    def _is_finished_log(log_fp):
        return parse_log(log_fp).state == 5

    def _is_finished_plot(plot_fp):
        for log_fp in filter(_is_finished_log, log_fps):
            if parse_log(log_fp).plot_id in plot_fp:
                return True
        return False

    # Step 1
    uncopied_finished_plots = [fp for fp in temp_fps if _is_finished_plot(fp)]
    if len(uncopied_finished_plots) > 0:
        print(f'Uncopied file detected:')
        for fp in uncopied_finished_plots:
            print(fp)
        return 1

    # Step 2
    print('Cleaning temp directories...')
    prompt_to_delete(temp_dirs, ignore_outer_directories=True)

    # Step 3
    orphan_log_fps = [fp for fp in log_fps if not _is_finished_log(fp)]
    print('Cleaning orphan logs...')
    prompt_to_delete(orphan_log_fps, ignore_outer_directories=True)


def run(temp_dir, dest_dir):
    command = gen_command(temp_dir, dest_dir)
    cur_time = dt.datetime.strftime(dt.datetime.now(), r'%Y-%m-%d-%H-%M-%S-%f')
    log_path = os.path.join(log_dir, cur_time + '.log')
    err_path = os.path.join(log_dir, cur_time + '.err')
    with open(log_path, 'w+') as f_log, open(err_path, 'w+') as f_err:
        proc = sp.Popen(command, stdout=f_log, stderr=f_err, shell=True)
        try:
            print(f'Start plotting @ {cur_time} -> {dest_dir}')
            return proc
        except:
            kill_proc_tree(proc.pid)


def main():
    temp_dirs = [job.temp_dir for job in jobs]

    if clear_temp_dirs(temp_dirs) == 1:
        return

    job_stats: Dict[Job, JobStat] = {}

    try:
        for job in jobs:
            proc = run(job.temp_dir, job.dest_dir)
            job_stats[job] = JobStat(proc, 0)
            time.sleep(delay)

        while len(job_stats) > 0:
            time.sleep(10)
            for job in list(job_stats.keys()):
                stat = job_stats[job]
                if stat.proc.poll() == 0:  # job done
                    stat.done += 1
                    if stat.done < job.times:  # continue
                        stat.proc = run(
                            job.temp_dir, job.dest_dir)
                    else:
                        del job_stats[job]
    except KeyboardInterrupt:
        pass
    finally:
        for job in job_stats:
            stat = job_stats[job]
            kill_proc_tree(stat.proc.pid)


if __name__ == '__main__':
    main()
