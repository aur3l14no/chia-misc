import subprocess as sp
from dataclasses import dataclass
from typing import Optional


@dataclass(frozen=True)
class Job:
    temp_dir: str
    dest_dir: str
    times: int
    post_cmd: Optional[str] = None

    def __hash__(self):
        return id(self)


@dataclass
class JobStat:
    proc: sp.Popen
    done: int
