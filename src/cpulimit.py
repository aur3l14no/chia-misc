'''CPU limit daemon (Linux Only)'''

import subprocess as sp
import time
import argparse
import datetime as dt
import logging

cpulimit_cmd = 'cpulimit -i -l 80 -p {pid}'

logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s", level=logging.INFO)

def start_limit():
    ps_plots = sp.run('ps -ef | grep "plots create"', capture_output=True, shell=True, encoding='utf8')
    for l in ps_plots.stdout.splitlines():
        if 'grep' in l:
            continue
        pid = l.split()[1]

        # Run cpulimit if not already
        ret_ps_cpulimit = sp.run(f'ps -ef | grep "{cpulimit_cmd.format(pid=pid)}"', capture_output=True, shell=True, encoding='utf8')
        already_cpulimit = False
        for l in ret_ps_cpulimit.stdout.splitlines():
            if 'grep' not in l:  # already there
                already_cpulimit = True
        if not already_cpulimit:
            logging.info(f'Start limiting {pid}')
            sp.Popen(cpulimit_cmd.format(pid=pid), shell=True)

def stop_limit():
    sp.Popen('killall cpulimit', shell=True)
    logging.info(f'Killall cpulimits')

def daemon():
    is_cpulimit_running = False
    while True:
        if dt.datetime.now().hour in (1,2,3):  # bed time, find plots proc and apply limits
            start_limit()
            is_cpulimit_running = True
        elif is_cpulimit_running:  # day time, kill all limits
            stop_limit()
            is_cpulimit_running = False
        time.sleep(10)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', action='store_true', dest='f', help='force cpulimit')
    parser.add_argument('-k', action='store_true', dest='k', help='killall cpulimit')
    args = parser.parse_args()
    if args.f:
        start_limit()
    elif args.k:
        stop_limit()
    else:
        daemon()
