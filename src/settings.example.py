from job import Job

fingerprint = '11111111'
n_threads = 2
bucket_size = 128
mem_size = 3500
log_dir = 'C:\\chia_logs'
chia_path = 'C:\\Users\\admin\\AppData\\Local\\chia-blockchain\\app-1.0.5\\resources\\app.asar.unpacked\\daemon\\chia.exe'
delay = 2  # delay between jobs

jobs = [
    # Job('F:\\plot_temp', 'F:\\plots', 1),
    # Job('F:\\plot_temp', 'F:\\plots', 1),
    Job('G:\\plot_temp', 'H:\\plots', 2),
    Job('I:\\plot_temp', 'J:\\plots', 2),
]

