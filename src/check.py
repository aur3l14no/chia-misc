import os
import argparse
from settings import log_dir
from log_parser import parse_log


def collect_log(args):
    if args.path != '':
        log_dir = args.path
    def _collect_time(fp):
        log_info = log_dict[fp]
        print(f'{fp} {log_info.plot_id}', end='')
        if log_info.total_time != None:
            print(f' Finished in {log_info.total_time}')
        else:
            print(f' Running at phase {log_info.state + 1}')
        for i in range(1, 5):
            t = log_info.__dict__[f'time_phase_{i}']
            if t != None:
                print(t, end='\t')
        print()

    def _collect_id(fp):
        log_info = log_dict[fp]
        print(f'{fp}: {log_info.plot_id}')

    log_dict = {}
    for fn in os.listdir(log_dir):
        fp = os.path.join(log_dir, fn)
        if fp.endswith('.log'):
            log_dict[fp] = parse_log(fp)
    log_fps = sorted(list(log_dict.keys()), reverse=True)

    if args.collect_finished_ids:
        list(map(_collect_id, filter(lambda fp: log_dict[fp].state == 5, log_fps)))
        return

    n_unfinished = len(list(map(_collect_time, filter(lambda fp: log_dict[fp].state != 5, log_fps))))
    list(map(_collect_time, log_fps[n_unfinished: args.num]))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--collect-finished-ids', action='store_true', dest='collect_finished_ids')
    parser.add_argument('-n', '--num', type=int, default=0, dest='num')
    parser.add_argument('--path', type=str, default='', dest='path')
    args = parser.parse_args()

    collect_log(args)
